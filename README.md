- prepare environment
`
npm i
APP_PORT=8080 AUTH_TOKEN=SECRET_SECRET_TOKEN ENSWITCH_AUTH_USERNAME=XXX ENSWITCH_AUTH_PASSWORD=XXX node proxy.js
`
- then api can be accessed via:
`
curl localhost:8080/api/json/cdr/list?limit=1
`
