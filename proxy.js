const http = require('http');
const axios = require('axios');

const onRequest = (request, response) => {
    if (!request.headers.token || request.headers.token !== process.env['AUTH_TOKEN']) {
        response.write(JSON.stringify({"status": "Auth is incorrect"}));
        response.end();
        return;
    }
    console.log(`Proxing ${request.method} ${request.url}`);
    let config = {
        url: `https://www.omega-telecom.net/${request.url}`,
        method: request.method,
        params: {
            auth_username: process.env['ENSWITCH_AUTH_USERNAME'],//need to replace with actual
            auth_password: process.env['ENSWITCH_AUTH_PASSWORD'],//need to replace with actual
            ...request.queryParams,
        },
    }
    if (request.data) {
        config['data'] = request.data;
    }
    axios
        .request(config)
        .then((res) => {
            Object.entries(res.headers).forEach(([key, value]) => {
                response.setHeader(key, value);
            })
            return res.data;
        })
        .then(json => {
            response.write(JSON.stringify(json));
            response.end();
        }).catch(e => {
        response.write(JSON.stringify(e.response.data));
        response.statusCode = e.response.status;
        response.end();
    });
};

http.createServer(onRequest).listen(process.env['APP_PORT']);